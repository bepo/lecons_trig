#!/usr/bin/python
# -*- coding: utf-8 -*-

#création de leçons de dactylographie basées sur des mots composés des trigrammes fréquents de la langue française
#usage python lecons_crea.py 4
#pour créer des lecons de 4 mots ou plus


import random

import sys


def getDic(n,start = 0,fName='',save=True,lang='fr'):
    if lang == 'fr':
        f = open("./dic.txt")
    else:
        f = open("./dic_en.txt")
    dic = []
    i = 0
    for l in f:
        i += 1
        if i>start:
            dic.append(l[0:-1])
            if len(dic)==n :
                if fName!='' and save:
                    g = open(fName,'w')
                    for j in dic:
                        g.write(j+'\r\n')
                    g.close()    
                return dic
    return dic

def checkMot(m,dic):
    #Vérifie si le mot est composé des trigrammes de dic, par récursivité (on ne se refait pas)
    # hyp : len(m)>3
    l = len(m)
    idx = 0
    s = m[idx:idx+3]
    #on vérifie le début du mot
    if s in dic:
        #mot de trois lettres, on sort
        if l == 3 :
            return True
        s1InDic = False
        s2InDic = False
        s3InDic = False
        #on teste si le trigramme suivant (à idx+3, le plus à droite possible) est dans le dictionnaire des trigrammes, si oui on passe au reste du mot par récursivité
        if l>=6:
            s3 = m[idx+3:idx+3+3]
            s3InDic = s3 in dic
            if (s3InDic and l == 6):
                return True
            elif s3InDic:
                return checkMot(m[3:],dic)
        #sinon on teste une lettre avant à idx+2, si oui on passe au reste du mot par récursivité
        if l>=5:            
            s2 = m[idx+2:idx+2+3]
            s2InDic = s2 in dic
            if (s2InDic and l == 5):
                return True
            elif s2InDic:
                return checkMot(m[2:],dic)
        #sinon on teste encore une lettre avant à idx+1, si oui on passe au reste du mot par récursivité
        if l>=4:
            s1 = m[idx+1:idx+1+3]
            s1InDic = s1 in dic
            if (s1InDic and l == 4):
                return True
            elif s1InDic:
                return checkMot(m[1:],dic)
    #les trois premières lettres ne forment pas un trigramme connu, on renvoit faux.
    return False
        
def treatFileMots(fname,dic,foutName = '',shuffleName = '',save=True):
    #Vérifie tous les mots d'un fichier
    f = open(fname)
    res = []
    for l in f:
        l = l.replace('\r\n','')
        l = l.replace('\n','')
        if len(l)>3 and l!='\r\n' and l!=' \r\n' and l!='' and checkMot(l,dic): #mot candidat 
            #vérifier que le mot trouvé n'est pas déjà inclus dans un mot de la solution
            found = False
            rem = []
            if len(res)>=1:
                for i in res:
                    if len(i)>len(l):
                        if i.find(l)!=-1:   #l est inclus dans i,  on ne prend pas l
                            found = True
                            #print (l+' est déjà dans '+i)   
                            continue
                    else:
                        if l.find(i)!=-1:   #i est inclus dans l, on vire i
                            #print (i+' est inclus dans '+l)   #il y a une erreur, on passe trop par ici
                            rem.append(i)
                for i in rem:
                    res.remove(i)
            if not found:
                res.append(l)
    if foutName !='' and save:
        fout = open(foutName,"w")
        for l in res:
            fout.write(l+"\r\n")
        fout.close()
    if shuffleName !=''and save:
        fout = open(shuffleName,"w")
        random.shuffle(res)
        for l in res:
            fout.write(l+"\r\n")
        fout.close()
    return res

    
def lecons(minMots = 3):
    racine = './'
    version = '05'
    m = 0
    dicFileName = ''
    # Choix de la langue de génération
    lang = 'fr'
#    lang = 'en'
    fullDic = getDic(450,0,dicFileName,False,lang)
    f  = open(racine+'sortie/lecons_'+version+'_'+str(minMots)+'_'+lang+'.txt',"w")
    f_ = open(racine+'sortie/lecons_'+version+'_'+str(minMots)+'_'+lang+'_.txt',"w")
    #print "fullDic",fullDic 
    nb = 3 #taille mini du dictionnaire de trigrammes
    countMots = 0
    countTrigs = 0
    for a in range(1,31):
        f.write('leçon '+str(a)+'\n')
        f_.write('leçon '+str(a)+'\n')
        res = []
        while len(res)<minMots and nb<len(fullDic):
            nb += 1
            dic = fullDic[0:nb]
            if lang=='fr':
                iFileName = racine+"lexiqueMots.txt"
                iFileName = racine+"liste.de.mots.francais.frgut.txt"
            else:
                iFileName = racine+"wordsEn.txt"
            oFileName = racine+'lecon_'+str(a)+'.txt'
            shuffleFileName = racine+'mots_shuffle_'+str(a)+'.txt'
            res = treatFileMots(iFileName,dic,oFileName,shuffleFileName,False)
        print (a, res, len(res))
        countMots += len(res)
        #trouver les trigrammes non utilisés
        used = []
        for i in range(0,len(res)):
            for j in dic:
                if res[i].find(j) >= 0 and j not in used :
                    used.append(j)
        f.write('mots basés sur :')
        f_.write('mots basés sur :')
        #print dic, len(dic)
        #print used, len(used)
        nb = len(dic) - len(used)
        countTrigs += nb
        for u in used:
            f.write(' '+u)
            f_.write(' '+u)
        f.write('\n')
        f_.write('\n')
        f.write(' ')
        for u in res:
            f.write(' '+u)
        f.write('\n')
 
        #un petit mélange pour éviter que les mots de la même famille se suivent
        random.shuffle(res)  
        for u in res:
            f_.write(u+'\n')
        
        for i in range(0,len(used)):
            if used[i] in fullDic:
                del fullDic[fullDic.index(used[i])]
        #print 'fullDic',fullDic
        if nb>=len(fullDic):
            f.close()
            f_.close()
            
            break
    #Attention Ntrigs est faux car parfois supérieur au nombre de trigrammes du dictionnaire
    print('Nmots :'+str(countMots)+', Ntrigs :'+str(countTrigs)+', ratio :'+str(float(countMots)/countTrigs))
    

def main():
    if (len(sys.argv) > 1):
        lecons(int(sys.argv[1]))
    else:
        lecons()
    
    
    
if __name__ == '__main__':
    main()
