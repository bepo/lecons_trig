#!/usr/bin/python
# -*- coding: utf-8 -*-


#Leçons pour dactylotest

import random


repetMot = 8        #À renseigner à la main
repetTrig = 10      #À renseigner à la main

#À renseigner à la main
f = open('./sortie/lecons_05_5_fr.txt')
fout = open('./sortie/leçons_dacty_05_5_fr.txt',"w")

while True :
    #ligne leçon
    l = f.readline()
    if l=='':
        fout.close()
        break
    #fout.write(l+'\n')
    fout.write(l)
    #ligne trigrammes
    l = f.readline()
    m = l.split(':')
    print (m)
    m = m[1].strip()
    fout.write(m+'\n')
    n = m.split()
    j = 0
    for o in n:
        inter = n[:]
        inter.remove(o)
        print(inter,o)
        for i in range(repetTrig):
            fout.write(o)
            if ((i+1)%3 == 0 and i!=repetTrig-1):
                fout.write(' '+inter[j%len(inter)])
                j = j+1
            if i == repetTrig -1 :
                fout.write('\n')
            else:
                fout.write(' ')
    fout.write(m+'\n')
    #ligne mots
    l = f.readline()
    m = l.strip()
    fout.write(m+'\n')
    n = m.split()
    for o in n:
        for i in range(repetMot):
            fout.write(o)
            if i == repetMot -1 :
                fout.write('\n')
            else:
                fout.write(' ')
    fout.write(m+'\n')
    
    
